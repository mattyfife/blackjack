public class Card
{
    String suit;
    String number;
    public Card(String number, String suit){
        this.number = number;
        this.suit = suit;
    }

    public String getNumber()
    {
        return this.number;
    }

    public String getSuit()
    {
        return this.suit;
    }
}