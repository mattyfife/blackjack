import java.util.*;
public class Valuation
{
    public static final Map<String, Integer> cardValueMappings = new HashMap<String, Integer>() {{
        put("two",   2);
        put("three", 3);
        put("four",  4);
        put("five",  5);
        put("six",   6);
        put("seven", 7);
        put("eight", 8);
        put("nine",  9);
        put("ten",   10);
        put("jack",  10);
        put("queen", 10);
        put("king",  10);
    }};
}