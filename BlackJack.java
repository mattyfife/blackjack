import java.util.*;
public class BlackJack
{
    public static void main(String[] args)
    {
        Player dealer = new Player("Dealer");
        Player billy = new Player("Billy");
        Player andrew = new Player("Andrew");
        Player carla = new Player("Carla");

        dealer.addCard(new Card("jack", "spades"));
        dealer.addCard(new Card("seven", "diamonds"));

        billy.addCard(new Card("two", "spades"));
        billy.addCard(new Card("two", "diamonds"));
        billy.addCard(new Card("two", "hearts"));
        billy.addCard(new Card("four", "diamonds"));
        billy.addCard(new Card("five", "clubs"));

        andrew.addCard(new Card("king", "diamonds"));
        andrew.addCard(new Card("four", "spades"));
        andrew.addCard(new Card("four", "clubs"));

        carla.addCard(new Card("queen", "clubs"));
        carla.addCard(new Card("six", "spades"));
        carla.addCard(new Card("nine", "diamonds"));

        int dealerVal = dealer.getCardValues();

        System.out.println("dealer : "  + dealerVal);
        System.out.println("Billy : "  + billy.getCardValues() + " [" + playerResult(dealerVal, billy) + "]");
        System.out.println("Andrew : " + andrew.getCardValues() + " [" + playerResult(dealerVal, andrew) + "]");
        System.out.println("Carla : "  + carla.getCardValues() + " [" + playerResult(dealerVal, carla) + "]");
        
    }

    public static String playerResult(int dealerVal, Player player)
    {
        int playerVal = player.getCardValues();
        
        if ( playerVal > 21 ) { return "loses"; }
        if ( playerVal <= 21 && player.getCardCount() >= 5 ) { return "beats dealer"; }
        if ( playerVal > dealerVal ) { return "beats dealer"; }
        return "loses";
    }

}