import java.util.*;
class Player
{
    String name;
    ArrayList<Card> cards;

    public Player (String name)
    {
        this.name = name;
        this.cards = new ArrayList<Card>();
    }

    public void addCard(Card c)
    {
        this.cards.add(c);
    }

    public ArrayList<Card> getCards() { return this.cards; }
    public String getName() { return this.name; }

    public int getCardValues()
    {
        return evaluateCardValues();
    }

    public int getCardCount()
    {
        return cards.size();
    }

    private int evaluateCardValues()
    {
        int val = 0;
        int aceCount = 0;
        for (Card c : cards)
        {
            if (c.getNumber().equals("ace"))
            {
                aceCount += 1;
            }
            else
            {
                val += Valuation.cardValueMappings.get(c.getNumber());
            }
        }
        switch (aceCount){
            case 0: break;
            case 1: 
                if (val + 11 > 21) { val += 1;} else { val += 11;} break;
            case 2:
                if (val + 11 + 1 > 21) { val += 2;} else { val += 12;} break;
            case 3:
                if (val + 11 + 1 + 1 > 21) { val += 3;} else { val += 13;} break;
            case 4:
                if (val + 11 + 1 + 1 + 1 > 21) { val += 4;} else { val += 14;} break;
            default: break;

        }
        return val;
    }
}

